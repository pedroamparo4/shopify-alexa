﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using AlexaAPI.Core;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using ShopifyAPI;
using ShopifyAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlexaAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AlexaController : Controller
    {

        [HttpPost]
        public SkillResponse HandleResponse([FromBody] SkillRequest input)
        {
            var requestType = input.GetRequestType();
            var session = input.Session;

            if (requestType == typeof(LaunchRequest))
            {
                return ResponseBuilder.Ask(ALEXA_SPEECHES.GetRandomHelloSpeech(), null);
            }
            else if (requestType == typeof(IntentRequest))
            {
                var intentRequest = input.Request as IntentRequest;

                switch (intentRequest.Intent.Name)
                {
                    case ALEXA_INTENTS.MARKET_SELECT:
                        return ManageSelectMarketIntent(intentRequest);
                    case ALEXA_INTENTS.ADD_PRODUCT_TO_CART:
                        return ManageAddingToCartIntent(intentRequest);
                    case ALEXA_INTENTS.GET_PRODUCT_PRICE:
                        return ManageGetProductPriceIntent(intentRequest);
                    case ALEXA_INTENTS.GET_PRODUCT_AVAILABILITY:
                        return ManageProductAvailabilityIntent(intentRequest);
                    case ALEXA_INTENTS.GET_CART_INFO:
                        return ManageGetCartInfoIntent(intentRequest);
                    case ALEXA_INTENTS.CHECKOUT:
                        return ManageCheckoutIntent(intentRequest);
                }
            }

            return ResponseBuilder.Ask(ALEXA_SPEECHES.GetRandomUnknownCommandSpeech(), null);
        }

        private SkillResponse ManageSelectMarketIntent(IntentRequest intent)
        {
            var marketId = intent.Intent.Slots["MarketName"].Resolution.Authorities[0].Values[0].Value.Id;

            if (CartSessionManager.SessionAvailable)
            {
                if(marketId == CartSessionManager.GetSelectedEntityShopId())
                {
                    return ResponseBuilder.Ask($"Ya tienes un carrito habilitado para comprar en {CartSessionManager.GetSelectedEntityShopName()}, ¿Deseas agregar algo al carrito?", null);
                }

                return ResponseBuilder.Ask($"Todavia no has finalizado tu compra en {CartSessionManager.GetSelectedEntityShopName()}, si deseas, puedes pedirme que realice el checkout de los productos o vaciar el carrito de compras antes de proceder a comprar en otro market", null);
            }

            if(CartSessionManager.SelectEntityShop(marketId))
            {
                return ResponseBuilder.Ask($"De acuerdo, ¿Qué deseas comprar en {CartSessionManager.GetSelectedEntityShopName()}?", null);
            }

            return ResponseBuilder.Ask("Lo siento, no reconozco ese Market, ¿puedes repetirme el nombre por favor?", null);

        }

        private SkillResponse ManageAddingToCartIntent(IntentRequest intent)
        {
            var cartService = new CartService();

            if (!CartSessionManager.SessionAvailable)
            {
                return ResponseBuilder.Ask("No has especificado un market para comprar, por favor, indícame el market en el cual deseas comprar", null);
            }

            if (intent.Intent.Slots["ProductQuantity"].Value == "?")
            {
                return ResponseBuilder.Ask("No entendí la cantidad que deseas agregar, ¿puedes repetir por favor?", null);
            }
            else if (intent.Intent.Slots["ProductName"].Value == "?")
            {
                return ResponseBuilder.Ask("No entendí el producto que deseas agregar, ¿puedes repetir por favor?", null);
            }
            else
            {
                var productQuantity = int.Parse(intent.Intent.Slots["ProductQuantity"].Value.ToString());
                var productId = intent.Intent.Slots["ProductName"].Resolution.Authorities[0].Values[0].Value.Id;
                var productInfo = cartService.GetProductInfo(new ShopifyAPI.Dtos.Product { InternalId = productId });

                cartService.AddProductToCart(new ShopifyAPI.Dtos.Product { Quantity = productQuantity, ShopifyId = productInfo.ShopifyId });
                return ResponseBuilder.Ask(ALEXA_SPEECHES.BUY_PRODUCT_SPEECHES.PRODUCT_ADDED_SUCCESFULLY.Replace("{ProductQuantity}", intent.Intent.Slots["ProductQuantity"].Value).Replace("{ProductName}", intent.Intent.Slots["ProductName"].Value), null);
            }
        }

        private SkillResponse ManageGetProductPriceIntent(IntentRequest intent)
        {

            var cartService = new CartService();

            if (!CartSessionManager.SessionAvailable)
            {
                return ResponseBuilder.Ask("No has especificado un market para comprar, por favor, indícame el market en el cual deseas comprar", null);
            }

            if (intent.Intent.Slots["ProductName"].Value == "?")
            {
                return ResponseBuilder.Ask("No entendí el producto que deseas consultar, ¿puedes repetir por favor?", null);
            }

            var productId = intent.Intent.Slots["ProductName"].Resolution.Authorities[0].Values[0].Value.Id;
            var productInfo = cartService.GetProductInfo(new ShopifyAPI.Dtos.Product { InternalId = productId });

            return ResponseBuilder.Ask($"{productInfo.Title} tiene un costo de {productInfo.Price} dólares por unidad", null);

        }

        private SkillResponse ManageProductAvailabilityIntent(IntentRequest intent)
        {

            var cartService = new CartService();

            if (!CartSessionManager.SessionAvailable)
            {
                return ResponseBuilder.Ask("No has especificado un market para comprar, por favor, indícame el market en el cual deseas comprar", null);
            }

            if (intent.Intent.Slots["ProductName"].Value == "?")
            {
                return ResponseBuilder.Ask("No entendí el producto que deseas consultar, ¿puedes repetir por favor?", null);
            }

            string response = string.Empty;
            var productId = intent.Intent.Slots["ProductName"].Resolution.Authorities[0].Values[0].Value.Id;
            var productInfo = cartService.GetProductInfo(new ShopifyAPI.Dtos.Product { InternalId = productId });

            if(productInfo.Available)
            {
                response = $"Según {CartSessionManager.GetSelectedEntityShopName()}, tienen disponibilidad para el producto {productInfo.Title}";
            }
            else
            {
                response = $"Según {CartSessionManager.GetSelectedEntityShopName()}, no tienen disponibilidad para el producto {productInfo.Title}";

            }

            return ResponseBuilder.Ask(response, null);

        }

        private SkillResponse ManageGetCartInfoIntent(IntentRequest intent)
        {

            var cartService = new CartService();

            if (!CartSessionManager.SessionAvailable)
            {
                return ResponseBuilder.Ask("No has especificado un market para comprar, por favor, indícame el market en el cual deseas comprar", null);
            }

            string productListString = string.Empty;
            var cartInfo = cartService.GetCartInfo();

            for(var i = 0; i < cartInfo.CartProducts.Count; i++)
            {
                productListString += $"{cartInfo.CartProducts[i].Quantity} {cartInfo.CartProducts[i].Title}";
                if(i < cartInfo.CartProducts.Count-1)
                {
                    productListString += ", ";
                }
            }
            productListString = productListString.Replace("- No Message", "");

            return ResponseBuilder.Ask($"Actualmente tiene un total a pagar de {cartInfo.TotalToPay} dólares, tiene agregado {productListString}", null);

        }

        private SkillResponse ManageCheckoutIntent(IntentRequest intent)
        {
            var cartService = new CartService();
            cartService.CloseEntityCart();
            return ResponseBuilder.Tell("De acuerdo, el pago se ha realizado exitósamente, la tienda te enviará la factura de tu compra al correo.", null);
        }

    }
}
