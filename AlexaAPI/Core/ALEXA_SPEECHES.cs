﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlexaAPI.Core
{
    static public class ALEXA_SPEECHES
    {
        public static string GetRandomHelloSpeech()
        {
            Random rnd = new Random();
            int index = rnd.Next(HELLO_SPEECHES.SPEECHES.Count);
            return HELLO_SPEECHES.SPEECHES[index];
        }

        public static string GetRandomUnknownCommandSpeech()
        {
            Random rnd = new Random();
            int index = rnd.Next(UNKNOWN_COMMAND_SPEECHES.SPEECHES.Count);
            return UNKNOWN_COMMAND_SPEECHES.SPEECHES[index];
        }

        public static class UNKNOWN_COMMAND_SPEECHES
        {
            public static List<string> SPEECHES = new List<string> { "No entendí, ¿Puedes repetir?", "¿Puedes repetir de forma más pausada por favor?", "Lo siento, ¿Puedes repetir por favor?" };
        }

        static public class HELLO_SPEECHES
        {
            public static List<string> SPEECHES = new List<string> { "Hola!, ¿En cuál market deseas comprar?, si quieres, puedo indicarte el listado de markets disponibles" }; 
        }

        static public class HELP_SPEECHES
        {
            public static string EXPLAIN_AVAILABLE_OPTIONS = "Puedes solicitar agregar un producto al carrito, por ejemplo: Alexa, agrega unas manzanas al carrito";
        }

        static public class BUY_PRODUCT_SPEECHES
        {
            public static string PRODUCT_ADDED_SUCCESFULLY = "Listo, he agregado {ProductQuantity} {ProductName} al carrito de compras, ¿Qué más deseas hacer?";
        }
    }
}
