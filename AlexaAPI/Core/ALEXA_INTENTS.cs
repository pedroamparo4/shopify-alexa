﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlexaAPI.Core
{
    static public class ALEXA_INTENTS
    {
        public const string ADD_PRODUCT_TO_CART = "AddProductCartIntent";

        public const string MARKET_SELECT = "MarketSelectIntent";

        public const string GET_PRODUCT_PRICE = "GetProductPriceIntent";

        public const string GET_PRODUCT_AVAILABILITY = "GetProductAvailabilityIntent";

        public const string GET_CART_INFO = "GetCartInfoIntent";

        public const string CHECKOUT = "CheckoutIntent";

    }
}
