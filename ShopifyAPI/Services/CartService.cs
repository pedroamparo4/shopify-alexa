﻿using Newtonsoft.Json;
using RestSharp;
using ShopifyAPI.Core;
using ShopifyAPI.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShopifyAPI.Services
{
    public class CartService
    {
        public CartService() { }

        public EntityShop SelectEntityShopCart(string entityShopId)
        {
            CloseEntityCart();
            CartSessionManager.SelectEntityShop(entityShopId);
            return GetSelectedEntityShop();
        }


        /// <summary>
        /// Método encargado de cerrar la sesión del Market que se encuentre abierto
        /// </summary>
        public void CloseEntityCart()
        {
            CartSessionManager.ClearCookies();
            CartSessionManager.UnSelectEntityShop();
        }

        /// <summary>
        /// Método utilizado para agregar un producto al carrito
        /// </summary>
        /// <param name="product"></param>
        public void ChangeProductQuantityInCart(Dtos.Product product)
        {
            var selectedEntityShop = GetSelectedEntityShop();

            var client = new RestClient($"{selectedEntityShop.Endpoint}/cart/change.js");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "ba3730d2-bc6a-8299-1daa-33d579c0d513");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"id={product.ShopifyId}&quantity={product.Quantity}", ParameterType.RequestBody);

            if (CartSessionManager.GetCookies() != null)
            {
                foreach (var cookie in CartSessionManager.GetCookies())
                {
                    request.AddCookie(cookie.Name, cookie.Value);
                }
            }

            IRestResponse response = client.Execute(request);
            CartSessionManager.UpdateCookies(response.Cookies);
        }

        /// <summary>
        /// Método utilizado para agregar un producto al carrito
        /// </summary>
        /// <param name="product"></param>
        public void AddProductToCart(Dtos.Product product)
        {
            var selectedEntityShop = GetSelectedEntityShop();

            var client = new RestClient($"{selectedEntityShop.Endpoint}/cart/add.js");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"items%5B0%5Did={product.ShopifyId}&items%5B0%5Dquantity={product.Quantity}", ParameterType.RequestBody);

            if (CartSessionManager.GetCookies() != null)
            {
                foreach (var cookie in CartSessionManager.GetCookies())
                {
                    request.AddCookie(cookie.Name, cookie.Value);
                }
            }

            IRestResponse response = client.Execute(request);
            CartSessionManager.UpdateCookies(response.Cookies);
        }

        public Dtos.Product GetProductInfo(Dtos.Product product)
        {
            dynamic result = null;
            var selectedEntityShop = GetSelectedEntityShop();
            var entityShopProduct = selectedEntityShop.Products.Where(x => x.InternalId == product.InternalId).FirstOrDefault();

            var client = new RestClient($"{selectedEntityShop.Endpoint}/products/{entityShopProduct.Handle}.js");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "d7a73c11-cdea-d4da-162b-17da43b364c7");
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);
            result = JsonConvert.DeserializeObject(response.Content);

            return new Dtos.Product
            {
                InternalId = entityShopProduct.InternalId,
                ShopifyId = entityShopProduct.ShopifyId,
                Title = result.title,
                Price = entityShopProduct.Price,
                Available = result.available
            };
        }

        private EntityShop GetSelectedEntityShop()
        {
            return EntitiesCollection.GetEntitiesCollection().Where(x => x.Id == CartSessionManager.GetSelectedEntityShopId()).FirstOrDefault();
        }

        public Cart GetCartInfo()
        {
            dynamic result = null;
            var output = new Cart();
            var selectedEntityShop = GetSelectedEntityShop();

            var client = new RestClient($"{selectedEntityShop.Endpoint}/cart.js");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");

            if (CartSessionManager.GetCookies() != null)
            {
                foreach (var cookie in CartSessionManager.GetCookies())
                {
                    request.AddCookie(cookie.Name, cookie.Value);
                }
            }

            IRestResponse response = client.Execute(request);
            CartSessionManager.UpdateCookies(response.Cookies);

            result = JsonConvert.DeserializeObject(response.Content);
            output.TotalToPay = ((decimal)result.total_price) / 100;

            foreach(var product in result.items)
            {
                output.CartProducts.Add(new Product 
                {
                  Title = product.title,
                  Quantity = product.quantity
                });
            }

            return output;
        }
    }
}
