﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopifyAPI.Core
{
    public class EntityShop
    {
        public EntityShop()
        {
            Products = new List<EntityShopProduct>();
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Endpoint { get; set; }

        public List<EntityShopProduct> Products { get; set; }
    }
}
