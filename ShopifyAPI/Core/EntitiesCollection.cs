﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopifyAPI.Core
{
    public static class EntitiesCollection
    {
        private static List<EntityShop> COLLECTION = null;

        public static List<EntityShop> GetEntitiesCollection()
        {
            if (COLLECTION == null)
            {
                BuildEntities();
            }

            return COLLECTION;
        }

        private static void BuildEntities()
        {
            COLLECTION = new List<EntityShop>();
            COLLECTION.Add(new EntityShop
            {
                Id = "1",
                Name = "The Humming Bird Bakery",
                Endpoint = "https://hummingbirdbakery.com",
                Products = new List<EntityShopProduct> {
                new EntityShopProduct { InternalId = "2.1", ShopifyId = "36005469776", Name = "Red Velvet Cupcake", Handle = "red-velvet-cupcake", Price = 2.95m },
                new EntityShopProduct { ShopifyId = "1504077676578", Name = "Rainbow Piñata Cake", Handle = "" },
                new EntityShopProduct { ShopifyId = "9097469200", Name = "Small Luxury Selection Box", Handle = "" },
                new EntityShopProduct { ShopifyId = "1699810181154", Name = "Maltesers Cheesecake", Handle = "" }
            }
        });
        }

    }
}
