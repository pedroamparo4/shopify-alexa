﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopifyAPI.Core
{
    public class EntityShopProduct
    {
        public string InternalId { get; set; }

        public string ShopifyId { get; set; }

        public string Name { get; set; }

        public string Handle { get; set; }

        public decimal Price { get; set; }
    }
}
