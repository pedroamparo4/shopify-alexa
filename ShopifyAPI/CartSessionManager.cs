﻿using RestSharp;
using ShopifyAPI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShopifyAPI
{
    public static class CartSessionManager
    {
        private static string SelectedEntityShopName = "";
        private static string SelectedEntityShopId = null;

        private static IList<RestResponseCookie> SessionCookies = null;

        public static bool SessionAvailable { get { return (SelectedEntityShopId != null);  } }

        public static void UpdateCookies(IList<RestResponseCookie> cookies)
        {
            SessionCookies = cookies;
        }

        public static IList<RestResponseCookie> GetCookies()
        {
            return SessionCookies;
        }

        public static bool SelectEntityShop(string entityShopId)
        {
            var entityShop = EntitiesCollection.GetEntitiesCollection().Where(x => x.Id == entityShopId).FirstOrDefault();

            if (entityShop != null)
            {
                SelectedEntityShopId = entityShopId;
                SelectedEntityShopName = entityShop.Name;
                return true;
            }

            return false;
        }

        public static string GetSelectedEntityShopId()
        {
            return SelectedEntityShopId;
        }

        public static string GetSelectedEntityShopName()
        {
            return SelectedEntityShopName;
        }

        public static void ClearCookies()
        {
            SessionCookies = null;
        }

        public static void UnSelectEntityShop()
        {
            SelectedEntityShopId = null;
            SelectedEntityShopName = null;
        }

    }
}
