﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopifyAPI.Dtos
{
    public class Product
    {
        public string InternalId { get; set; }

        public string ShopifyId { get; set; }

        public decimal Price { get; set; }

        public string Title { get; set; }

        public string Handle { get; set; }

        public bool Available { get; set; }

        public int Quantity { get; set; }

    }
}
