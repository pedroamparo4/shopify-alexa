﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopifyAPI.Dtos
{
    public class Cart
    {
        public Cart()
        {
            CartProducts = new List<Product>();
        }

        public decimal TotalToPay { get; set; }

        public List<Product> CartProducts { get; set; }
    }
}
